/*
Copyright © CINES - Centre Informatique National pour l'Enseignement Supérieur (2020) 

[dad@cines.fr]

This software is a computer program whose purpose is to provide 
a web application to create, edit, import and export archive 
profiles based on the french SEDA standard
(https://redirect.francearchives.fr/seda/).


This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

package fr.gouv.vitamui.pastis.controller;

import fr.gouv.vitamui.pastis.model.ElementProperties;
import fr.gouv.vitamui.pastis.model.jaxb.*;
import fr.gouv.vitamui.pastis.util.PastisCustomCharacterEscapeHandler;
import fr.gouv.vitamui.pastis.util.PastisGetXmlJsonTree;
import fr.gouv.vitamui.pastis.util.PastisSAX2Handler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.net.URISyntaxException;

@RestController
public class ProfileController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProfileController.class);

    private static final String APPLICATION_JSON_UTF8 = "application/json; charset=utf-8";

    @Value("${rng.base.file}")
    private String rngFile;

    @RequestMapping(value = "/updateprofile", method = RequestMethod.POST, consumes = APPLICATION_JSON_UTF8, produces = "application/xml")
    public String updateprofile(@RequestBody final ElementProperties json) throws IOException  {

        // Recover a statically generated BaliseXML by buildBaliseXMLTree
        json.initTree(json);
        BaliseXML.buildBaliseXMLTree(json,0, null);
        BaliseXML eparentRng  = BaliseXML.baliseXMLStatic;
        String response = null;
        Writer writer = null;
        try {
            JAXBContext contextObj = JAXBContext.newInstance(AttributeXML.class, ElementXML.class, DataXML.class,
                    ValueXML.class, OptionalXML.class, OneOrMoreXML.class,
                    ZeroOrMoreXML.class, AnnotationXML.class, DocumentationXML.class,
                    StartXML.class, GrammarXML.class,ChoiceXml.class);
            Marshaller marshallerObj = contextObj.createMarshaller();
            marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshallerObj.setProperty("com.sun.xml.bind.marshaller.CharacterEscapeHandler",
                    new PastisCustomCharacterEscapeHandler());

            ByteArrayOutputStream os = new ByteArrayOutputStream();
            writer = new OutputStreamWriter(os, "UTF-8");

            marshallerObj.marshal(eparentRng, writer);
            response = new String (os.toByteArray(), "UTF-8");

        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (JAXBException e1) {
            e1.printStackTrace();
        }
        finally {
            writer.close();
        }

        LOGGER.info("RNG profile generated successfully");
        return response;
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test() {

        return "Pastis API is listening...";
    }

    @RequestMapping (value = "/getfile", method = RequestMethod.GET, produces = "text/plain")
    public ResponseEntity<String> getFile() {

        InputStream rngFile = getClass().getClassLoader().getResourceAsStream("profile3.rng");
        if (rngFile != null) {
            return new ResponseEntity<>(rngFile.toString(), HttpStatus.OK);
        }	else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping (value = "/createprofile", method = RequestMethod.GET)
    public ResponseEntity<String> createprofile() throws URISyntaxException {
        PastisSAX2Handler handler = new PastisSAX2Handler();
        PastisGetXmlJsonTree getJson = new PastisGetXmlJsonTree();

        try {
            XMLReader xmlReader = XMLReaderFactory.createXMLReader();
            xmlReader.setContentHandler(handler);

            LOGGER.info("Starting rng edition profile with base file : {}", this.rngFile);

            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(this.rngFile);
            InputSource inputSource = new InputSource(inputStream);

            xmlReader.parse(inputSource);

        } catch (SAXException | IOException e  ) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(getJson.getJsonParsedTree(handler.elementRNGRoot));
    }


    @RequestMapping (value = "/createprofilefromfile", method = RequestMethod.POST, consumes = "multipart/form-data", produces = "application/json")
    public ResponseEntity<String> createprofilefromfile(@RequestParam MultipartFile file )  {

        PastisSAX2Handler handler = new PastisSAX2Handler();
        PastisGetXmlJsonTree getJson = new PastisGetXmlJsonTree();

        try {
            XMLReader xmlReader = XMLReaderFactory.createXMLReader();
            xmlReader.setContentHandler(handler);
            xmlReader.parse(new InputSource(file.getInputStream()));
        } catch (IOException e) {
            return new ResponseEntity<>("Error while processing file : ", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (SAXException e) {
            if (e instanceof SAXParseException) {
                return new ResponseEntity<>("Erreur lors du chargement du profil, ligne " + ((SAXParseException) e).getLineNumber() + " colonne " + ((SAXParseException) e).getColumnNumber() + ": " + e.getMessage() , HttpStatus.INTERNAL_SERVER_ERROR);
            } else {
                return new ResponseEntity<>("Error while processing file : ", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return ResponseEntity.ok(getJson.getJsonParsedTree(handler.elementRNGRoot));
    }
}
