/*
Copyright © CINES - Centre Informatique National pour l'Enseignement Supérieur (2020)

[dad@cines.fr]

This software is a computer program whose purpose is to provide
a web application to create, edit, import and export archive
profiles based on the french SEDA standard
(https://redirect.francearchives.fr/seda/).


This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

import { Given, When, Then, Before, After, setDefaultTimeout } from 'cucumber';
import { PastisPage } from '../pages/pastis.po';
import * as chai from 'chai';
import chaiAsPromised = require('chai-as-promised');
import { browser, element, ElementFinder } from 'protractor';
import { FileTreeNode } from '../pages/file-tree-node.po';
import { Metadata } from '../pages/metadata.po';
import AsyncUtils from '../class/async-utils';

const pastis  = new PastisPage();
const expect = chai.expect;
const assert = chai.assert;

Before(() => {
  chai.use(chaiAsPromised);
  browser.manage().window().maximize();
});

// Timeout assez long, permettant de debug le code sans 'cramer' les promises qui attendent.
setDefaultTimeout(60 * 80085);


Given('The user goes to the homepage', async function () {
    console.log('===The user goes to the homepage');
    await pastis.navigateTo("");
    await pastis.waitForSpinner();
});

Then('The user clicks on {string} tab', async function (string) {
    console.log('===The user clicks on ' + string +' tab');
    await pastis.leftPanel.FileTreeTab.getTabByName(string).click();
});

Then('The user should see the {string} tab', async function (tabName) {
    console.log('===The user should see the ' + tabName +' tab');
    let title: string = '';
    switch(tabName){
        case 'ENTÊTE': title = 'Entête';break;
        case 'RÈGLES': title = 'Règles';break;
        case 'ARBORESCENCE': title = "Unités d'archives";break;
        case 'OBJETS': title = 'Objets';break;
    }
    expect(await pastis.leftPanel.FileTreeTabBody.getTitle()==title).to.be.true;
});

When('The user clicks on the export button', async function () {
    console.log('===The user clicks on the export button');
    pastis.deleteAlreadyDownloadedFiles();
    await pastis.buttonMenu.ExportProfileButton.click();
});

Then('The RNG file is downloaded', async function () {
    console.log('===The RNG file is downloaded');
    expect(await pastis.verifyFileDownload()).to.be.true;
});

Then('The user navigates to {string}', async function (nodesName) {
    console.log('===The user navigates to ' + nodesName);
    let node:FileTreeNode = pastis.leftPanel.FileTreeTabBody.getFileTreeNodes();
    let names: string[] = nodesName.split(',')
    let parent:FileTreeNode; 
    for(let i = 0; i<names.length; i++) {
        if (!parent){
            parent = node;
        }
        parent = (await AsyncUtils.find(await parent.getChildren(), async (f)=>(await f.getName())==names[i]));
        await parent.container.click();
    }
});

Then('The user writes {string} in the field {string} of the metadata {string}', async function (text, fieldName, metadataName) {
    console.log('===The user writes ' + text + ' in the field ' + fieldName + ' of the metadata ' + metadataName);
    let metadata: Metadata = await pastis.metadataTable.metadataTableBody.getMetadataByName(metadataName);
    let field: ElementFinder;
    switch(fieldName){
        case 'valeurFixe': field = metadata.valeurFixe.container;break;
        case 'cardinalite': field = metadata.cardinalite.container;break;
        case 'commentaire': field = metadata.commentaire.container;break;
    }
    await field.sendKeys(text);
});

Then('The user clicks on Ajouter une métadonnée', async function () {
    console.log('===The user clicks on Ajouter une métadonnée');
    await pastis.metadataTable.addMetadataButton.click();
});

Then('The user adds the metadata {string}', async function(metadataName){
    console.log('===The user adds the metadata ' + metadataName);
    await (await AsyncUtils.find(await pastis.metadataPopup.getMetadataSearch(), async (m)=>{return await m.getName()==metadataName})).addButton.click();
});

Then('The user validates the popup', async function(){
    console.log('===The user validates the popup');
    await pastis.metadataPopup.validate();
});

Then('The user cancels the popup', async function(){
    console.log('===The user cancels the popup');
    await pastis.metadataPopup.cancel();
});

Then('Verify that the list of nodes of {string} is {string}', async function(nodesName, values){
    console.log('===Verify that the list of nodes of ' + nodesName + ' is ' + values);
    let node:FileTreeNode = pastis.leftPanel.FileTreeTabBody.getFileTreeNodes();
    let names: string[] = nodesName.split(',')
    let parent:FileTreeNode; 
    for(let i = 0; i<names.length; i++) {
        if (!parent){
            parent = node;
        }
        parent = await AsyncUtils.find(await parent.getChildren(), async c=>{return await c.getName()==names[i]});
    }
    let nodesNames: string[] = await AsyncUtils.map(await parent.getChildren(), async c=>{return await c.getName()});
    assert.deepEqual(nodesNames.sort(), values.split(',').sort());
});

Then('Verify that the value of the field {string} of the metadata {string} is {string}', async function (field, metadataName, value) {
    console.log('===Verify that the value of the field ' + field + ' of the metadata ' + metadataName + ' is ' + value);
    let nodeValue:string = await (await pastis.metadataTable.metadataTableBody.getMetadataByName(metadataName)).getfieldByName(field).getValue();
    expect(nodeValue).to.equals(value);
});

Then('Verify that the list of options of the field {string} of the metadata {string} is {string}', async function (field, metadataName, values) {
    console.log('===Verify that the list of options of the field ' + field + ' of the metadata ' + metadataName + ' is ' + values);
    let nodeValue: string[] = await (await pastis.metadataTable.metadataTableBody.getMetadataByName(metadataName)).getfieldByName(field).getOptions();
    assert.deepEqual(nodeValue.sort(), values.split(',').sort());
});

Then('Open the menu of the metadata {string}', async function (metadataName){
    console.log('===Open the menu of the metadata ' + metadataName);
    await (await pastis.metadataTable.metadataTableBody.getMetadataByName(metadataName)).openContextMenu();
});

Then('Close the popup', async function (){
    console.log('===Close the popup');
    await pastis.currentOpenedPopup.close()
});

Then('Verify that the list of options in the context menu of the metadata {string} is {string}', async function (metadataName, values){
    console.log('===Verify that the list of options in the context menu of the metadata ' + metadataName + ' is ' + values);
    let labels: string[] = await pastis.metadataContextMenu.getMenuLabelList();
    assert.deepEqual(labels.sort(), values.split(',').sort());
});

Then('The user clicks on the item {string} of the context menu', async function (item:string){
    console.log('===The user clicks on the item ' + item + ' of the context menu');
    await (await pastis.metadataContextMenu.getMenuOptionByLabel(item)).click();
});

Then('Verify that the list of attributes is {string}', async function(attributes){
    console.log('===Verify that the list of attributes is ' + attributes);
    let attributs: string[] = await AsyncUtils.map(await pastis.attributesPopup.attributesTableBody.getAttributes(), async a =>{return await a.getName()});
    assert.deepEqual(attributs.sort(), attributes.split(',').sort()); 
});

Then('The user writes {string} in the field {string} of the attribute {string}', async function(text,fieldName,attributeName){
    console.log('===The user writes ' + text + ' in the field ' + fieldName + ' of the attribute ' + attributeName);
    let field = (await pastis.attributesPopup.attributesTableBody.getAttributeByName(attributeName)).getfieldByName(fieldName);
    await field.container.sendKeys(text);
});

Then('Verify that the value of the field {string} of the attribute {string} is {string}', async function(fieldName,attributeName,text){
    console.log('===Verify that the value of the field ' + fieldName + ' of the attribute ' + attributeName + ' is ' + text);
    let field =  (await pastis.attributesPopup.attributesTableBody.getAttributeByName(attributeName)).getfieldByName(fieldName);
    assert.deepEqual(await field.getValue(),text);
});

Then('The user clicks on Tout sélectionner', async function(){
    console.log('===The user clicks on Tout sélectionner');
    await pastis.attributesPopup.attributesTableHeader.selectAllCheckbox.click();
});

Then('The user selects the attributes {string}', async function(attributes:string){
    console.log('===The user selects the attributes ' + attributes);
    await AsyncUtils.forEach(attributes.split(','),async (attr:string) => {(await pastis.attributesPopup.attributesTableBody.getAttributeByName(attr)).checkbox.click();});
});

Then('Verify that the selected attributes are {string}', async function (expectedAttributes){
    console.log('===Verify that the selected attributes are ' + expectedAttributes);
    let attributselected: string[] = await AsyncUtils.map(await pastis.attributesPopup.attributesTableBody.getSelectedAttributes(), async attr=>{return await attr.getName()});
    let atts = [];
    if (expectedAttributes != ''){
        atts = expectedAttributes.split(',');
    }
    expect(attributselected).to.deep.equal(atts);
});

Then('Verify that the list of options of the field {string} of the attribute {string} is {string}', async function(fieldName,attributeName,options){
    console.log('===Verify that the list of options of the field ' + fieldName + ' of the attribute ' + attributeName + ' is ' + options);
    let field =  (await pastis.attributesPopup.attributesTableBody.getAttributeByName(attributeName)).getfieldByName(fieldName);
    assert.deepEqual((await field.getOptions()).sort(), options.split(',').sort());
});

After(function (test) {
    // Todo catch these *** exceptions
    browser.manage().addCookie({ name: 'zaleniumTestPassed', value: test.result.status == 'failed' ? 'false' : 'true' });
})