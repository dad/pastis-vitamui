/*
Copyright © CINES - Centre Informatique National pour l'Enseignement Supérieur (2020) 

[dad@cines.fr]

This software is a computer program whose purpose is to provide 
a web application to create, edit, import and export archive 
profiles based on the french SEDA standard
(https://redirect.francearchives.fr/seda/).


This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ToggleSidenavService } from '../../core/services/toggle-sidenav.service';
import { FileService } from '../../core/services/file.service';
import { SedaService } from '../../core/services/seda.service';

import { FileNode } from '../../file-tree/classes/file-node';
import { MatTabChangeEvent, MatTreeNestedDataSource } from '@angular/material';
import { NestedTreeControl } from '@angular/cdk/tree';
import { BehaviorSubject } from 'rxjs';
import { FileTreeComponent } from '../../file-tree/file-tree.component';
import { SedaData } from '../../file-tree/classes/seda-data';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'pastis-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss',],
  //encapsulation: ViewEncapsulation.None
})

export class EditProfileComponent implements OnInit, OnDestroy {

  nodeToSend: FileNode;

  sedaParentNode: SedaData;

  profileRulesIsLoaded: boolean;
  
  loadRules: boolean;

  activeTabIndex: number;

  sideNavOpened: boolean;

  tabRootElementName:string;

  tabRulesMap: Map<string, Map<string, string[]>>;
  nodeParentChildMap: Map<string, string[]>;

  nestedTreeControl: NestedTreeControl<FileNode>;
  nestedDataSource: MatTreeNestedDataSource<FileNode>;

  dataChange = new BehaviorSubject<FileNode[]>([]);

  profileTabChildrenToInclude: string[] = [];
  profileTabChildrenToExclude: string[] = [];
  headerTabChildrenToInclude: string[] = [];
  headerTabChildrenToExclude: string[] = [];
  rulesTabChildrenToInclude: string[] = [];
  rulesTabChildrenToExclude: string[] = [];
  treeTabChildrenToInclude: string[] = [];
  treeTabChildrenToExclude: string[] = [];
  objectTabChildrenToInclude: string[] = [];
  objectTabChildrenToExclude: string[] = [];

  rootNames: string[] = [];
  displayedRootNames: string[] = [];
  tabLabels: string[] = [];
  collectionNames: string[] = [];
  tabShowElementRules : string [][][]= [];

  @ViewChild(FileTreeComponent, {static: false}) fileTreeComponent: FileTreeComponent;

  constructor(private sedaService:SedaService, private fileService: FileService, 
    private sideNavService: ToggleSidenavService, private loaderService:NgxUiLoaderService) {

    this.nestedTreeControl = new NestedTreeControl<FileNode>(this.getChildren);
    this.nestedDataSource = new MatTreeNestedDataSource();

    this.tabLabels.push('PROFIL','ENTÊTE','RÈGLES','ARBORESCENCE','OBJETS');
    this.collectionNames = this.tabLabels.map(name=>name.charAt(0).toUpperCase() + name.slice(1).toLowerCase());
    this.rootNames.push('','ArchiveTransfer','ManagementMetadata','DescriptiveMetadata','DataObjectPackage');
    this.displayedRootNames.push('','Entête','Règles','Unités d\'archives','Objets');
    
    // Children to include or exclude
    this.profileTabChildrenToInclude.push();
    this.profileTabChildrenToExclude.push();
    this.headerTabChildrenToInclude.push();
    this.headerTabChildrenToExclude.push('DataObjectPackage','DataObjectGroup','DescriptiveMetadata','ManagementMetadata','id','BinaryDataObject');
    this.rulesTabChildrenToInclude.push();
    this.rulesTabChildrenToExclude.push();
    this.treeTabChildrenToInclude.push();
    this.treeTabChildrenToExclude.push();
    this.objectTabChildrenToInclude.push('BinaryDataObject','PhysicalDataObject')
    this.objectTabChildrenToExclude.push('ManagementMetadata','ArchiveUnit','DescriptiveMetadata');
    this.tabShowElementRules.push(
      [this.profileTabChildrenToInclude,this.profileTabChildrenToExclude],
      [this.headerTabChildrenToInclude,this.headerTabChildrenToExclude],
      [this.rulesTabChildrenToInclude,this.rulesTabChildrenToExclude],
      [this.treeTabChildrenToInclude,this.treeTabChildrenToExclude],
      [this.objectTabChildrenToInclude,this.objectTabChildrenToExclude])
  }
  ngOnInit() {
    this.activeTabIndex = 1;
    this.isTabClicked(this.activeTabIndex)
    this.fileService.setCollectionName(this.collectionNames[this.activeTabIndex]);
    this.fileService.setTabRootMetadataName(this.rootNames[this.activeTabIndex]);
    this.fileService.setNewChildrenRules(this.tabShowElementRules[1])
    this.fileService.getFileTreeFromApi().subscribe(response => {
      if (response) {
        this.nodeToSend = response[0];
        if (this.nodeToSend){
          this.fileService.allData.next(response);
          let fiteredData = this.getFilteredData(this.activeTabIndex);
          this.nestedDataSource.data = fiteredData;
          this.nestedTreeControl.dataNodes = fiteredData;
          this.nestedTreeControl.expand(fiteredData[0]);
          this.dataChange.next(fiteredData);
          this.fileService.filteredNode.next(fiteredData[0]);
        }
        console.log("Init file tree node on file tree : %o", this.dataChange.getValue()[0]);
      }
    });
    this.sedaService.getSedaRules().subscribe(data=>{
        this.sedaParentNode = data[0];
    })
  }

  isTabClicked(i: number): boolean {
    return i === this.activeTabIndex;
  }

  loadProfile(event: MatTabChangeEvent) {
    this.activeTabIndex = event.index;
    this.loadProfileData(event.index);
  }

  loadProfileData(index:number) {
    this.fileService.setCollectionName(this.collectionNames[this.activeTabIndex])
    this.fileService.setTabRootMetadataName(this.rootNames[this.activeTabIndex]);
    console.log("On load tab for ", this.fileService.collectionName.getValue());
    let nodeToFilter = this.rootNames[index];
    let nodeToExpand = this.rootNames[index];
    this.fileService.setNewChildrenRules(this.tabShowElementRules[index])
    let fiteredData = this.getFilteredData(index);
    if (fiteredData[0]) {
      this.loaderService.start();
      this.fileService.nodeChange.next(fiteredData[0]);
      this.nestedDataSource.data = fiteredData;
      this.nestedTreeControl.dataNodes = fiteredData;
      this.nestedTreeControl.expand(fiteredData[0]);
      this.sedaService.selectedSedaNodeParent.next(this.sedaService.getSedaNodeRecursively(this.sedaParentNode,nodeToFilter))
      console.log("Root seda is ", this.sedaService.selectedSedaNodeParent.getValue());
      this.fileTreeComponent.sendNodeMetadata(fiteredData[0]);
    }
    this.loaderService.stop();

  }

  getFilteredData(index:number):FileNode[]{
    if(this.nodeToSend) {
      let nodeNameToFilter = this.rootNames[index];
      let currentNode = this.fileService.getFileNodeLocally(this.fileService.allData.getValue()[0],nodeNameToFilter);
      let filteredData = [];
      filteredData.push(currentNode);
      return filteredData;
    }
  }

  getChildren = (node: FileNode) => node.children;


  closeSideNav(){
    this.sideNavService.hide()
  }

  ngOnDestroy() {
  }

}
