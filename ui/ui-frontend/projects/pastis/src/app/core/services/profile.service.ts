/*
Copyright © CINES - Centre Informatique National pour l'Enseignement Supérieur (2020) 

[dad@cines.fr]

This software is a computer program whose purpose is to provide 
a web application to create, edit, import and export archive 
profiles based on the french SEDA standard
(https://redirect.francearchives.fr/seda/).


This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
import { HttpHeaders } from '@angular/common/http';
import { Injectable, isDevMode } from '@angular/core';
import { Observable } from 'rxjs';
import { FileNode } from '../../file-tree/classes/file-node';
import { PastisApiService } from './api.pastis.service';
import { PastisConfiguration } from '../classes/pastis-configuration';
import { environment} from '../../../environments/environment'
import { cloneDeep } from 'lodash';
@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  
  private apiServerPath: string;
  

  constructor(private apiService: PastisApiService, private pastisConfig: PastisConfiguration) {
      this.apiServerPath = isDevMode() ? environment.apiServerUrl : pastisConfig.apiPastisUrl;
  }

  getProfile(): Observable<FileNode[]> {
    console.error("API PATH : ", this.apiServerPath, " DEV MODE : ", isDevMode());
    return this.apiService.get<FileNode[]>(this.apiServerPath + this.pastisConfig.getProfileUrl);
  }

  // Upload a RNG file to the server
  // Response : a JSON object
  uploadProfile(profile: FormData): Observable<FileNode[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'multipart/form-data',
      })
    };
    console.log("Path : ", this.apiServerPath+this.pastisConfig.uploadProfileUrl)
    return this.apiService.post(this.pastisConfig.uploadProfileUrl, profile);
  }

  // Get the base rng profile
  getFile(): Observable<Blob> {
    const options = {responseType: 'blob'};
    return this.apiService.get(this.apiServerPath+this.pastisConfig.getFileUrl, options);
  }

  // Get the modified tree as an RNG file
  updateFile(file: FileNode[]): Observable<Blob> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
      }),
      responseType: 'blob'
    };
    let profile: FileNode = cloneDeep(file[0]);
    this.fixCircularReference(profile);
    return this.apiService.post(this.pastisConfig.updateFileUrl, profile, httpOptions);
  }  

  fixCircularReference(node: FileNode){
    node.parent=null;
    node.sedaData=null;
    node.children.forEach(child=>{this.fixCircularReference(child);});
  }
}
