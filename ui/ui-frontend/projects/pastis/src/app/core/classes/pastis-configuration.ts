import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { PastisApiService } from '../services/api.pastis.service';

@Injectable()

export class PastisConfiguration {
    routeName : string;
    sucessMessage: string;
    errorMessage: string;
    apiRoutePath : string;
    apiPastisUrl: string;
    port: number;
    getProfileUrl: string;
    uploadProfileUrl: string;
    getFileUrl: string;
    updateFileUrl: string;
    apiFullPath: string;

  constructor(private pastisApi: PastisApiService,private http:HttpClient){};

public ensureInit(): Promise<any> {
    return new Promise((r, e) => {
        this.pastisApi.get("./assets/config/config.json").subscribe((content: PastisConfiguration) => {Object.assign(this, content);r(this);}, reason => e(reason));
    });
  };

}
